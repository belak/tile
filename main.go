package main

import (
	"flag"
	"fmt"
	"github.com/BurntSushi/xgb/xproto"
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
	"github.com/BurntSushi/xgbutil/xprop"
)

/*
 * TODO:
 * - sort windows based on location
 */

// Converts an xorg cardinal to a uint
func cardinalToUint(data []byte) uint {
	// This looks awful, but it's really just starting
	// at the back and adding the values then bitshifting
	var ret uint
	for i := len(data) - 1; i >= 0; i-- {
		ret += uint(data[i])
		if i > 0 {
			ret = ret << 1
		}
	}
	return ret
}

func main() {
	fmt.Println("tile v0.1")

	// Setup command line flags and parse them
	var mode string
	flag.StringVar(&mode, "mode", "vertical", "the tiling mode")
	flag.Parse()

	util, err := xgbutil.NewConn()
	if err != nil {
		fmt.Println(err)
		return
	}

	// Get all clients
	win, err := ewmh.ClientListGet(util)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Get the current desktop
	desktop, err := ewmh.CurrentDesktopGet(util)
	fmt.Printf("Current desktop: %d\n", desktop)

	// Filter out windows on other desktops
	var windows []xproto.Window
	for _, id := range win {
		resp, err := xprop.GetProperty(util, id, "_NET_WM_DESKTOP")
		if err != nil {
			fmt.Println(err)
			//return
		}

		// TODO: Maybe multiple bytes are so you can have more than 255 desktops?
		if err != nil || cardinalToUint(resp.Value) != desktop {
			//fmt.Printf("Window on other desktop: %+v\n", resp.Value[0])
			continue
		}

		resp, err = xprop.GetProperty(util, id, "WM_NAME")
		if err != nil {
			fmt.Println(err)
			//return
		}
		fmt.Printf("Client(%d): %s\n", id, resp.Value)

		windows = append(windows, id)
	}

	// Get workarea info
	work, err := ewmh.WorkareaGet(util)
	if err != nil {
		fmt.Println(err)
		return
	}

	// If we have a workarea for this desktop, use it
	if uint(len(work)) <= desktop {
		fmt.Printf("Can't get workarea info for desktop %d\n", desktop)
		return
	}

	area := work[desktop]
	fmt.Printf("%+v\n", area)

	// Move the windows
	if len(windows) == 1 {
		err := ewmh.WmStateReqExtra(util, windows[0], ewmh.StateAdd, "_NET_WM_STATE_MAXIMIZED_HORZ", "_NET_WM_STATE_MAXIMIZED_VERT", 1)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		i := 0
		for _, window := range windows {
			// TODO: Use the xcbutil xwindow size stuff
			resp, err := xprop.GetProperty(util, window, "_NET_FRAME_EXTENTS")

			var left uint
			var right uint
			var top uint
			var bot uint

			if err == nil {
				left = cardinalToUint(resp.Value[0:4])
				right = cardinalToUint(resp.Value[4:8])
				top = cardinalToUint(resp.Value[8:12])
				bot = cardinalToUint(resp.Value[12:16])
			}

			lr := int(left + right)
			tb := int(top + bot)

			if mode == "horizontal" {
				winWidth := int(area.Width) / len(windows)
				err = ewmh.MoveresizeWindow(util, window, area.X+i*winWidth, 0, winWidth-lr, int(area.Height)-tb)
			} else if mode == "vertical" {
				winHeight := int(area.Height) / len(windows)
				err = ewmh.MoveresizeWindow(util, window, 0, area.Y+i*winHeight, int(area.Width)-int(left+right), winHeight-int(top+bot))
			} else if mode == "tile" {
				winHeight := int(area.Height) / (len(windows) - 1)

				if i == 0 {
					err = ewmh.MoveresizeWindow(
						util,
						window,
						0, 0,
						int(area.Width)/2-int(left+right),
						int(area.Height)-int(top+bot))
				} else {
					err = ewmh.MoveresizeWindow(
						util,
						window,
						int(area.Width)/2,
						area.Y+(i-1)*winHeight,
						int(area.Width)/2-int(left+right),
						winHeight-int(top+bot))
				}
			}

			if err != nil {
				fmt.Println(err)
			}

			i++
		}
	}
}
